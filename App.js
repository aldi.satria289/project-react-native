import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Welcome from './Components/Welcome'
import Login from './Components/Login'
import Register from './Components/Register'
import Home from './Components/Home'
import Details from './Components/Details'
import About from './Components/About'
import Navigation from './Navigation/index'
// import Flatlist from './Components/Flatlist'

export default function App() {
  return (
    // <Welcome/>
    // <Login/>
    // <Register/>
    // <Home/>
    // <Details/>
    // <About/>
    <Navigation/>
    // <Flatlist/>
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
