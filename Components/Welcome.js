import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, Dimensions } from 'react-native'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
              <View style={styles.welcomeBox}>
                  <Text style={styles.welcomeText}>Welcome to</Text>
                  <Text style={{fontSize:38, fontWeight:'bold', color:'#fff'}}>Kumpul News</Text>
              </View>
              <Image source={require('./images/news.png')} style={{marginTop:100, marginBottom:100, width:width, height:260}} resizeMode="stretch"/>
              <View style={styles.whiteBox}>
                  <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Login')}>
                  <Text style={styles.loginBox}>Masuk</Text></TouchableOpacity>
                  {/* <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Register')}>
                  <Text style={styles.registerBox}>Daftar</Text></TouchableOpacity> */}
              </View>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      width: width,
      height: height,
      backgroundColor: '#563D8E',
      justifyContent: 'center',
      alignItems: 'center',
    },
    welcomeBox: {
      width: 285,
      height: 121,
      backgroundColor: '#925BFF',
      paddingLeft: 20,
      marginTop: 140,
      justifyContent: 'center',
    },
    welcomeText: {
      fontFamily: 'Roboto',
      fontSize: 20,
      fontWeight: 'bold',
      color: '#fff'
    },
    whiteBox: {
      width: width,
      height: 200,
      backgroundColor: 'white',
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
      alignItems: 'center',
    },
    loginBox: {
      width: 326,
      fontFamily: 'Roboto',
      color: 'white',
      fontSize: 24,
      textAlign: "center",
      backgroundColor: '#FF8500',
      borderRadius: 10,
      marginTop: 60,
      marginBottom: 15,
      padding: 15,
    },
    registerBox: {
      width: 326,
      fontFamily: 'Roboto',
      color: 'white',
      fontSize: 24,
      textAlign: "center",
      backgroundColor: '#555',
      borderRadius: 10,
      margin: 15,
      padding: 15,
    },
  })

