import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, Dimensions, Linking } from 'react-native'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
              <View style={styles.messageBox}>
                  <Text style={styles.messageText}>5 Smartphone Oppo yang Rilis pada Kuartal IV Tahun 2020</Text>
              </View>
              <View style={styles.whiteBox}>
                  <View style={styles.newsInfo}>
                    <Text style={styles.newsTeksInfo}>Rabu, 25 November 2020</Text>
                    <Text style={styles.newsTeksInfo}>Author Artha Widiprasetyo</Text>
                  </View>
                  <Image source={require('./images/2.jpg')} style={styles.newsImage}/>
                  <View style={styles.newsContainer}>
                    <Text style={styles.newsDesc}>Pecinta Oppo merapat!</Text>
                    <Text style={styles.newsContent}>Tech Gadget | Perusahaan asal Tiongkok ini nampaknya semakin gencar dalam merilis smartphone baru, bahkan di penghujung tahun 2020 ini. Tak hanya kelas entry-level saja, tetapi juga kelas mid-range maupun flagship. Maka tak heran, penjualan smartphone Oppo bisa dibilang sangat stabil dan selalu meningkat dari tahun ke tahun… </Text>
                    <TouchableOpacity
                    onPress={() => {Linking.openURL('https://www.idntimes.com/tech/gadget/defender-widi/smartphone-oppo-yang-rilis-pada-kuartal-iv-tahun-2020-c1c2') }}>
                    <Text style={styles.readmore}>Baca Selengkapnya</Text></TouchableOpacity>
                  </View>
              </View>
        </View>
      )
    }
  }
  

  const styles = StyleSheet.create({
    container: {
      width: width,
      height: height,
      backgroundColor: '#563D8E',
      justifyContent: 'center',
      alignItems: 'center',
    },
    messageBox: {
      width: '90%',
      height: 46,
      backgroundColor: '#925BFF',
      borderLeftColor: '#FF8500',
      borderLeftWidth: 3,
      marginTop: 100,
      marginBottom: 54,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 5
    },
    messageText: {
      fontFamily: 'Roboto',
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'justify',
      color: '#fff'
    },
    whiteBox: {
      width: width,
      height: 800,
      backgroundColor: 'white',
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
      alignItems: 'center',
    },
    newsContainer: {
      width: width,
      alignSelf: 'center',
      padding: 15
    },
    newsDesc:{
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 12,
      marginBottom: 15,
    },
    newsContent:{
      textAlign: 'justify',
      fontSize: 16,
      fontFamily: 'Roboto',
      alignSelf: 'center',
    },
    newsImage:{
      width : 380,
      height: 200,
      marginTop: 15
    },
    newsTitle: {
      width: 300,
      textAlign: 'justify',
      fontSize: 14,
      marginLeft: 10,
    },
    newsInfo: {
      alignSelf: 'flex-end',
      marginRight: 20,
      marginTop: 10,
      textAlign: 'left',
    },
    readmore: {
      width: 250,
      fontFamily: 'Roboto',
      color: 'white',
      fontSize: 20,
      textAlign: "center",
      backgroundColor: '#FF8500',
      borderRadius: 10,
      marginTop: 40,
      marginBottom: 15,
      padding: 10,
      alignSelf: 'center',
    },
})

