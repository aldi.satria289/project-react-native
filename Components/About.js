import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, Dimensions } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'; 

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class App extends Component {
    render() {
      return (
        // <View style={styles.container}>
        <View>
              {/* <View style={styles.messageBox}>
                  <Text style={styles.messageText}>About Me</Text>
              </View> */}
              <View style={styles.whiteBox}>
                  <Image source={require('./images/Foto.jpg')} style={{marginTop:50, marginBottom:15, width:250,height:250,borderRadius:150}}/>
                  <View style={styles.textContainer}>
                    <Text style={styles.textKet}>Name :</Text>
                    <Text style={styles.textIsi}>ALDI SATRIA BUDI</Text>
                    <Text style={styles.textKet}>Origin :</Text>
                    <Text style={styles.textIsi}>MEDAN, INDONESIA</Text>
                  </View>
                  <View style={styles.kontakIsi}>
                        <View style={styles.kontakItem}>
                            <TouchableOpacity>
                                <FontAwesome name="facebook" size={50} style={{margin:20}} color="#FF8500" />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <FontAwesome name="instagram" size={50} style={{margin:20}} color="#FF8500" />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <FontAwesome name="twitter" size={50} style={{margin:20}} color="#FF8500" />
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <FontAwesome name="gitlab" size={50} style={{margin:20}} color="#FF8500" />
                            </TouchableOpacity>
                          </View>
                    </View>
                </View>   
        </View>
      )
    }
  }
  

  const styles = StyleSheet.create({
    container: {
      width: width,
      height: height,
      backgroundColor: '#563D8E',
      justifyContent: 'center',
      alignItems: 'center',
    },
    messageBox: {
      width: 285,
      height: 46,
      backgroundColor: '#925BFF',
      borderLeftColor: '#FF8500',
      borderLeftWidth: 3,
      paddingLeft: 10,
      marginTop: 100,
      marginBottom: 30,
      marginLeft: -100,
      justifyContent: 'center',
    },
    messageText: {
      fontFamily: 'Roboto',
      fontSize: 24,
      fontWeight: 'bold',
      textAlign: 'justify',
      color: '#fff'
    },
    whiteBox: {
      width: width,
      height: 800,
      backgroundColor: 'white',
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
      alignItems: 'center',
    },
    textContainer: {
      width: width,
      alignSelf: 'center',
      padding: 15
    },
    textKet:{
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontSize: 14,
      marginBottom: 5
    },
    textIsi:{
      textAlign: 'center',
      fontSize: 16,
      fontWeight: 'bold',
      fontFamily: 'Roboto',
      marginBottom: 10
    },
    kontakIsi: {
      flexDirection: 'column',
      justifyContent: 'space-between'
    },
    kontakItem: {
      flexDirection: 'row',
      justifyContent:'center'
    }
})

