import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, Dimensions } from 'react-native'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
              <View style={styles.messageBox}>
                  <Text style={styles.messageText}>Lets Start</Text>
              </View>
              <Image source={require('./images/news.png')} style={{marginTop:30, marginBottom:30, width:width, height:260}} resizeMode="stretch"/>
              <View style={styles.whiteBox}>
                  <Text style={styles.textSign}>Sign up</Text>
                  <TouchableOpacity>
                  <Text style={styles.textBox}>Name</Text></TouchableOpacity>
                  <TouchableOpacity>
                  <Text style={styles.textBox}>Email</Text></TouchableOpacity>
                  <TouchableOpacity>
                  <Text style={styles.textBox}>Phone Number</Text></TouchableOpacity>
                  <TouchableOpacity>
                  <Text style={styles.textBox}>Password</Text></TouchableOpacity>
                  <View style={styles.enter}>
                    <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Login')}>
                    <Text style={styles.belum}>Belum punya Akun?</Text></TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Drawer')}>
                    <Text style={styles.oke}>Masuk</Text></TouchableOpacity> 
                  </View>
              </View>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      width: width,
      height: height,
      backgroundColor: '#563D8E',
      justifyContent: 'center',
      alignItems: 'center',
    },
    messageBox: {
      width: 285,
      height: 46,
      backgroundColor: '#925BFF',
      paddingLeft: 10,
      marginTop: 120,
      marginLeft: -100,
      justifyContent: 'center',
    },
    messageText: {
      fontFamily: 'Roboto',
      fontSize: 24,
      fontWeight: 'bold',
      color: '#fff'
    },
    whiteBox: {
      width: width,
      height: 500,
      backgroundColor: 'white',
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
      alignItems: 'center',
    },
    textSign: {
      width: 326,
      fontFamily: 'Roboto',
      color: '#000',
      fontSize: 30,
      fontWeight: 'bold',
      marginTop: 30,
      marginBottom: 5,
    },
    textBox: {
      width: 326,
      fontFamily: 'Roboto',
      color: '#939393',
      fontSize: 16,
      borderBottomColor: '#000',
      borderBottomWidth: 1,
      marginTop: 10,
      marginBottom: 10,
      padding: 15,
    },
    enter: {
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    belum: {
      fontFamily: 'Roboto',
      color: 'white',
      fontSize: 14,
      fontWeight: 'bold',
      color: '#555',
      textAlign: "center",
      margin: 10
    },
    oke: {
      width: 122,
      fontFamily: 'Roboto',
      color: 'white',
      fontSize: 20,
      textAlign: "center",
      backgroundColor: '#FF8500',
      borderRadius: 10,
      marginLeft: 40,
      padding: 10,
    },
  })

