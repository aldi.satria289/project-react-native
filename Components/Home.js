import React, { Component } from 'react'
import { View, StyleSheet, Image, Text, TouchableOpacity, Dimensions, ActivityIndicator, useEffect, ScrollView, TouchableOpacityBase } from 'react-native'

// import {Details, News} from './Screen'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: null,
        }
    }

    componentDidMount () {
        return fetch('http://newsapi.org/v2/top-headlines?country=id&apiKey=9377ceb451944f6497dbcc69d4191700')
        .then ((response) => response.json())
        .then ((responseJson) => {
            this.setState({
                isLoading: false,
                dataSource: responseJson.articles,
            })
        })
        .catch((error) =>{
            console.log(error)
        })
    }


    render() {
        if(this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator/>
                </View>
            )
        } else {

        let article = this.state.dataSource.map((val, key) => {
            return <View key={key}>
                <View style={styles.newsContainer}>
                <Image source={{uri:val.urlToImage}} style={{width:80, height:45}}/>
                  <TouchableOpacity >
                  <Text style={styles.newsTitle}>{val.title}</Text>
                  </TouchableOpacity>
                </View>
            </View>
        })
        

      return (
        <View>
          {/* <View style={styles.container}> */}
              {/* <View style={styles.messageBox}>
                <Text style={styles.messageText}>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('Drawer')}>
                    <FontAwesome name="bars" size={28} color="#333" />
                  </TouchableOpacity>  
                Welcome</Text>
              </View> */}
              <View style={styles.whiteBox}>
                  <Text style={styles.textSign}>Today's News</Text>
                  <ScrollView style={styles.scroll}>{article}</ScrollView>
                  {/* <Image source={require('./images/1.jpg')} style={{width:80, height:45}}/>
                  <TouchableOpacity>
                  <Text style={styles.newsTitle}>Spesifikasi Lengkap dan Harga Oppo A15 di Indonesia - Kompas.com - Tekno Kompas.com</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={styles.newsContainer}>
                  <Image source={require('./images/2.jpg')} style={{width:80, height:45}}/>
                  <TouchableOpacity>
                  <Text style={styles.newsTitle}>PS5 Jadi Konsol Game Gaib, Sony Janji Tambah Stok - CNBC Indonesia</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={styles.newsContainer}>
                  <Image source={require('./images/1.jpg')} style={{width:80, height:45}}/>
                  <TouchableOpacity>
                  <Text style={styles.newsTitle}>Ini yang Terjadi Saat Puncak Gerhana Bulan Penumbra - JPNN.com</Text>
                  </TouchableOpacity>
                  </View>
                  <View style={styles.newsContainer}>
                  <Image source={require('./images/2.jpg')} style={{width:80, height:45}}/>
                  <TouchableOpacity>
                  <Text style={styles.newsTitle}>Studi Temukan Bukti Mars Pernah Dilanda Banjir Besar - Republika Online</Text>
                  </TouchableOpacity> */}
              </View>
        </View>
        )}
    }
  }
  

  const styles = StyleSheet.create({
    container: {
      width: width,
      height: height,
      backgroundColor: '#563D8E',
      justifyContent: 'center',
      alignItems: 'center',
    },
    loading: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    messageBox: {
      width: 285,
      height: 46,
      backgroundColor: '#925BFF',
      borderLeftColor: '#FF8500',
      borderLeftWidth: 3,
      paddingLeft: 10,
      marginTop: 100,
      marginBottom: 30,
      marginLeft: -100,
      justifyContent: 'center',
    },
    messageText: {
      fontFamily: 'Roboto',
      fontSize: 24,
      fontWeight: 'bold',
      color: '#fff'
    },
    whiteBox: {
      width: width,
      height: 800,
      backgroundColor: 'white',
      borderTopLeftRadius: 25,
      borderTopRightRadius: 25,
      alignItems: 'center',
    },
    textSign: {
      width: 326,
      fontFamily: 'Roboto',
      color: '#000',
      fontSize: 24,
      fontWeight: 'bold',
      marginTop: 15,
      marginBottom: 10,
      textAlign: "center",
    },
    newsContainer: {
      width: width,
      flexDirection:'row',
      justifyContent: 'center',
      paddingHorizontal: 15,
      marginTop: 10,
    },
    newsTitle: {
      width: 300,
      textAlign: 'justify',
      fontSize: 14,
      marginLeft: 10,
    },
    scroll: {
      marginBottom: 50,
    }
  })

