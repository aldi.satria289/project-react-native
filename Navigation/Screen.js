import React from "react";
import { View, Text, StyleSheet, Button, ScreenView, Alert, Dimensions, TouchableOpacity, TouchableOpacityBase } from "react-native";
import { FontAwesome } from '@expo/vector-icons'; 
import HomeScreen from '../Components/Home';
import DetailScreen from '../Components/Details';
import AboutScreen from '../Components/About';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Details = ({route}) => (
  <ScreenContainer>
    <Text style={{marginBottom:10}}>Detail Halaman</Text>
    <DetailScreen/>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);


export const News = ({navigation}) => (
  <ScreenContainer>
    <View style={styles.messageBox1}>
      <TouchableOpacity style={{marginRight: 5}} onPress={() => navigation.toggleDrawer()}>
        <FontAwesome name="bars" size={28} color="#333" /></TouchableOpacity>  
      <Text style={styles.messageText}>Welcome</Text>
    </View>
    <View style={styles.button}>
    <Button color="#FF8500" title="Lihat Detail" onPress={() => navigation.push("Details", {name: "Ini Project Gw"})} />
    </View>
    <HomeScreen/>
  </ScreenContainer>
);

export const About = ({navigation}) => (
  <ScreenContainer>
    <View style={styles.messageBox}>
      <TouchableOpacity style={{marginRight: 5}} onPress={() => navigation.toggleDrawer()}>
        <FontAwesome name="bars" size={28} color="#333" /></TouchableOpacity>  
      <Text style={styles.messageText}>About</Text>
    </View>
    <AboutScreen/>
  </ScreenContainer>
);

const styles = StyleSheet.create({
  container: {
    width: width,
    height: height,
    backgroundColor: '#563D8E',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  messageBox: {
    width: 285,
    height: 46,
    backgroundColor: '#925BFF',
    borderLeftColor: '#FF8500',
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginTop: 100,
    marginBottom: 54,
    marginLeft: -100,
    flexDirection: 'row',
    padding: 5
  },
  messageBox1: {
    width: 285,
    height: 46,
    backgroundColor: '#925BFF',
    borderLeftColor: '#FF8500',
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginTop: 100,
    marginLeft: -100,
    flexDirection: 'row',
    padding: 5
  },
  messageText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    marginLeft: 5,
    fontWeight: 'bold',
    color: '#fff'
  },
});