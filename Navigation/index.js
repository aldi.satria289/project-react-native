import React from "react";
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs'
import {createDrawerNavigator} from '@react-navigation/drawer'
import {enableScreens} from 'react-native-screens';

import Login from '../Components/Login';
import RegisterScreen from '../Components/Register';
import WelcomeScreen from '../Components/Welcome';
import AboutScreen from '../Components/About';
import HomeScreen from '../Components/Home';
import DetailScreen from '../Components/Details';

import {Details, News, About} from './Screen'

import { StyleSheet, Text, View } from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';

enableScreens();

const HomeStack = createStackNavigator();
const HomeStackScreen = () => (
  <HomeStack.Navigator headerMode="none">
    <HomeStack.Screen name="Home" component={News} options={{ headerTitleAlign: 'center' }}/>
    <HomeStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </HomeStack.Navigator>
)

const ProjectStack = createStackNavigator();
const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="Project" component={Project} options={{ headerTitleAlign: 'center' }}/>
    <ProjectStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </ProjectStack.Navigator>
)

const AddStack = createStackNavigator();
const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="Add" component={Add} options={{ headerTitleAlign: 'center' }}/>
    <AddStack.Screen 
      name="Details" 
      component={Details} 
      options={({ route }) => ({
        title: route.params.name
      })}/>
  </AddStack.Navigator>
)

const WelcomeStack = createStackNavigator();
const WelcomeStackScreen = () => (
  <WelcomeStack.Navigator headerMode="none">
    <WelcomeStack.Screen name="Kumpul News" component={WelcomeScreen} options={{ headerTitleAlign: 'center' }}/>
  </WelcomeStack.Navigator>
)

const LoginStack = createStackNavigator();
const LoginStackScreen = () => (
  <LoginStack.Navigator headerMode="none">
    <LoginStack.Screen name="Login" component={Login} options={{ headerTitleAlign: 'center' }}/>
  </LoginStack.Navigator>
)

const RegisterStack = createStackNavigator();
const RegisterStackScreen = () => (
  <RegisterStack.Navigator headerMode="none">
    <RegisterStack.Screen name="Register" component={RegisterScreen} options={{ headerTitleAlign: 'center' }}/>
  </RegisterStack.Navigator>
)

const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
  <AboutStack.Navigator headerMode="none">
    <AboutStack.Screen name="About" component={About} options={{ headerTitleAlign: 'center' }}/>
  </AboutStack.Navigator>
)


const Tabs = createMaterialBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator
  initialRouteName='Home'
  activeColor="#694FAD"
  inactiveColor="#FF8500"
  barStyle={{ backgroundColor: '#FFF' }}
    >
      <Tabs.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tabs.Screen
        name="About"
        component={AboutStackScreen}
        options={{
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={TabsScreen}/>
    <Drawer.Screen name="Keluar" component={WelcomeStackScreen}/>
  </Drawer.Navigator>
)

const DetailStack = createStackNavigator();
const DetailStackScreen = () => (
  <DetailStack.Navigator headerMode="none">
    <DetailStack.Screen name="Detail" component={DetailScreen} options={{ headerTitleAlign: 'center' }}/>
    <DetailStack.Screen name="Tabs" component={TabsScreen}/>
    <DetailStack.Screen name="Drawer" component={DrawerScreen} />
  </DetailStack.Navigator>
)

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="Welcome" component={WelcomeStackScreen}/>
    <RootStack.Screen name="Home" component={HomeStackScreen}/>
    <RootStack.Screen name="Detail" component={DetailStackScreen}/>
    <RootStack.Screen name="Login" component={LoginStackScreen}/>
    <RootStack.Screen name="Register" component={RegisterStackScreen}/>
    <RootStack.Screen name="Drawer" component={DrawerScreen} />
  </RootStack.Navigator>
);

export default () => 
  (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});